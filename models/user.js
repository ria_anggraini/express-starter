'use strict';
module.exports = (sequelize, DataTypes) => {
  var user = sequelize.define('user', {
    first_name: {
      type : DataTypes.STRING(12),
      allowNull : false
    },
    last_name: {
      type : DataTypes.STRING(12),
      allowNull : false
    },
    DOB:{
      type: DataTypes.DATEONLY(12),
      allowNull:false
    },
    created_at :{
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW
    },
    updated_at :{
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW
    }
  }, {
    timestamps :false

  });
  user.associate = function(models) {
    // associations can be defined here
  };
  return user;
};