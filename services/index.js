const sequelize = require('sequelize')

module.exports ={
    post: async function (req, res ,model) {
        let transaction;
        try {  

            let body = req.body
            //start the transaction
            transaction = await model.sequelize.transaction()

            //save data to database
            await model.create(body, {transaction})
            
            //data commited if succes
            await transaction.commit()
            
        } catch (err) {
            
            await transaction.rollback();
        }

    },
}
