const express = require('express');
const router = express.Router();
const { check, validationResult } = require('express-validator/check');

const indexService = require('../services/index')
const db = require('../models/index')

router.post('/',

//define the vallidations here
[
check('fistname','minimum letter in first name should be 2 chars').isLength({min:2})
], (req, res) => {
  const model = db.user;
  indexService.post(req, res, model);
});

module.exports = router;
